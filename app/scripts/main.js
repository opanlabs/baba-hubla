$(document).ready(function() {
	
	$('ul.list-unstyled').on('click', '.init', function() {
      $(this).closest('ul.list-unstyled').children('li:not(.init)').toggle();
  	});

  var allOptions = $('ul.list-unstyled').children('li:not(.init)');
  $('ul.list-unstyled').on('click', 'li:not(.init)', function() {
      allOptions.removeClass('selected');
      $(this).addClass('selected');
      $('ul.list-unstyled').children('.init').html($(this).html());
      allOptions.toggle();
  });

   $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    focusOnSelect: true,
    dots: false,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });

  $('.single-item').slick({
    dots: true
  });

});	